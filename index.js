const fs = require('fs');
const https = require('https');

const ROOTURL = 'https://testserver.zekro.de';
const LOOPS = 50;

var resposneLog = [];

function getRandomNumber(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
}

function requestFile() {
    let fileName = `${ROOTURL}/${getRandomNumber(0, 4)}.png`;    
    console.log(`Accessing file '${fileName}'...`);
    https.get(fileName, () => {}).on('error', (e) => {
        console.error(e);
    });
}

function loop(i) {
    let timeout = 100 * getRandomNumber(1, 10);
    console.log(`[ ROUND ${i}/${LOOPS} ]\nWaiting for ${timeout} milliseconds...`)
    setTimeout(() => {
        requestFile();
        if (i >= LOOPS)
            return;
        loop(i + 1);
    }, timeout);
}

process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = '0';
loop(0);